module.exports = {
  settings: { // 在eslintrc.js中指定react版本
    react: {
      version: 'detect'
    }
  },
  plugins: ['react'], // 配置插件
  parser: '@typescript-eslint/parser', // 指定自定义解析器为ts-eslint
  extends: ['plugin:prettier/recommended'], // 继承 扩展配置文件
  parserOptions: {
    parser: 'babel-eslint',
    // 指定解析器选项
    ecmaVersion: 6,
    sourceType: 'module', // 默认script，可选module
    ecmaFeatures: {
      // 其他语言特性
      experimentalObjectRestSpread: true, // ...rest参数和扩展扩算符
      jsx: true,
      modules: true,
    },
  },
  env: {
    // 指定环境
    es6: true,
    node: true,
    browser: true,
  },
  // 布尔值false和字符串值"readable"等价于"readonly"
  // 布尔值true和字符串值"writeable"等价于"writable"
  globals: {
    // 指定全局变量
    React: true,
    document: false,
    navigator: false,
    window: false,
    DEV: true,
    TEST: true,
    PRE: true,
    PRO: true,
  },
  // 添加eslint自定义的规范（和prettier不冲突的规范）
  rules: {
    /**
     * @description 规则 ID 可以设置的值
     * “off” 或 0 - 关闭规则
     * “warn” 或 1 - 开启规则，使用警告级别的错误：warn (不会导致程序退出)
     * “error” 或 2 - 开启规则，使用错误级别的错误：error (当被触发的时候，程序会退出)
     */

    /**
     *  Possible Errors
     *  这些规则与 JavaScript 代码中可能的错误或逻辑错误有关
     *  https://cn.eslint.org/docs/rules/#possible-errors
     */
    'no-cond-assign': 2, // 条件不能有赋值
    'no-control-regex': 2, // reg中不能有控制符号
    'no-console': process.env.NODE_ENV === 'production' ? 1 : 0, // node环境prod禁用
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0, // node环境prod禁用
    'no-dupe-args': 2, // 参数不能重复
    'no-dupe-keys': 2, // 对象key不能重复
    'no-duplicate-case': 2, // switch不能有重复case
    'no-empty-character-class': 2, // reg中不能有空字符串
    'no-ex-assign': 2, // 不能复制catch中的error
    'no-extra-boolean-cast': 2, // 禁止多余的Boolean转换
    'no-extra-parens': [2, 'functions'], // 函数中禁止多余的括号
    'no-func-assign': 2, // 禁止赋值函数
    'no-inner-declarations': [2, 'functions'], // 禁止在条件中声明function
    'no-invalid-regexp': 2, // 禁止无用的reg
    'no-irregular-whitespace': 2, // 禁止不规则空格
    'no-unsafe-negation': 2, // !(key in object)->yes, !key in object->no
    'no-obj-calls': 2, // 禁止calling全局对象属性，如Math/JSON
    'no-regex-spaces': 2, // 禁止reg出现多个空格
    'no-sparse-arrays': 2, // array不能用空元素
    'no-unexpected-multiline': 2, // 禁止有疑义的多行表达式
    'no-unreachable': 2, // return/throw等之后不应有表达式
    'no-unsafe-finally': 2, // 禁止不安全的finally
    'use-isnan': 2, // 使用isNaN
    'valid-typeof': 2, // typeof的字符串必须正确

    /**
     *  Best Practices
     *  这些规则是关于最佳实践的，帮助你避免一些问题
     *  https://cn.eslint.org/docs/rules/#best-practices
     */
    'accessor-pairs': 2, // getter/setter成对出现
    'dot-location': [2, 'property'], // .的位置可以在换行
    eqeqeq: [2, 'allow-null'], // 必须===, null除外
    'no-caller': 2, // 禁止caller/callee
    'no-empty-pattern': 2, // 解构不能有空解构模式
    'no-eval': 2, // 不能用eval
    'no-extend-native': 2, // 不能扩展Object原型
    'no-extra-bind': 2, // bind的函数体中要有明确的this
    'no-fallthrough': 2, // switch需要break
    'no-floating-decimal': 2, // float中0不能省略
    'no-implied-eval': 2, // 禁止隐性eval
    'no-iterator': 2, // 禁止使用__iterator__属性
    'no-labels': [2, { allowLoop: false, allowSwitch: false }], // 禁止label表达式
    'no-lone-blocks': 2, // 禁止无用的{}
    'no-multi-spaces': 1, // 禁止多空格
    'no-multi-str': 2, // 禁止多行的string
    'no-global-assign': 2, // 禁止赋值原生对象(window/Object...)
    'no-new-wrappers': 2, // String/Number等不能用new
    'no-octal': 2, // 禁止八进制文字
    'no-octal-escape': 2, // 禁止八进制转义
    'no-proto': 2, // 禁止使用__proto__
    'no-redeclare': 2, // 禁止重新复制var
    'no-return-assign': [2, 'except-parens'], // 禁止return中赋值
    'no-self-assign': 2, // 禁止自身赋值
    'no-self-compare': 2, // 禁止自身比较, 如果NaN->Number.isNaN
    'no-sequences': 2, // 禁止,操作符
    'no-throw-literal': 2, // 禁止直接throw内容，必须是Error()
    'no-unmodified-loop-condition': 2, // 循环中的变量要在循环中修改
    'no-useless-call': 2, // 禁止无用的call
    'no-with': 2, // 禁用with
    'wrap-iife': [2, 'any'], // 立即调用的function必须有括号
    yoda: [2, 'never'], // 条件中变量在前
    'no-useless-escape': 0, // 不检查escape
    'no-new-func': 0, // 禁止对 Function 对象使用 new 操作符
    'no-param-reassign': 0, // 禁止对 function 的参数进行重新赋值
    'consistent-return': 0, // 要求 return 语句要么总是指定返回的值，要么不指定
    'class-methods-use-this': 0, // 强制类方法使用 this
    'array-callback-return': 0, // 数值循环显示return
    'no-unused-expressions': 0, // func && func()
    'no-loop-func': 0, // 禁止在loop内写循环函数

    /**
     *  Variables
     *  这些规则与变量声明有关
     *  https://cn.eslint.org/docs/rules/#variables
     */
    'no-delete-var': 2, // 不能delete变量，可以用在对象
    'no-label-var': 2, // 禁止label var
    'no-shadow-restricted-names': 2, // 禁止跟踪严格模式下部分关键词
    'no-restricted-globals': 0, // 禁用特定的全局变量
    // 'no-undef': 2, // 禁止使用未赋值变量
    'no-undef-init': 2, // 变量不能初始化为undefined
    'no-unused-vars': [0, { vars: 'all', args: 'none' }], // 禁止不使用的变量，参数可以
    'no-use-before-define': [2, { functions: false, classes: true, variables: true }], // 未定义不能使用
    'no-shadow': 0, // 禁止变量声明与外层作用域的变量同名

    /**
     *  ECMAScript 6
     *  这些规则只与 ES6 有关, 即通常所说的 ES2015
     *  https://cn.eslint.org/docs/rules/#ecmascript-6
     */
    'arrow-spacing': [1, { before: true, after: true }], // 箭头函数前后有空格
    'constructor-super': 1, // super()在必须构造函数内
    curly: [2, 'multi-line'], // if/while等函数可以多行不带{}
    'generator-star-spacing': [2, { before: true, after: true }], // generator函数前后有空格
    'no-class-assign': 2, // 禁止赋值class
    'no-const-assign': 2, // 禁止赋值常量(const)
    'no-dupe-class-members': 2, // class中方法不能有重复
    'no-new-symbol': 2, // new Symbol(xxx)->no
    'no-this-before-super': 2, // super()前不能用this
    'no-useless-computed-key': 2, // 禁止无用的计算属性
    'no-useless-constructor': 2, // 禁止无用的constructor
    'template-curly-spacing': [2, 'never'], // 模版字符串中变量无空格
    'yield-star-spacing': [2, 'both'], // yield的*前后有空格
    'prefer-const': 0, // 能用const场景用const
    'arrow-parens': [2, 'as-needed'], // 要求箭头函数的参数使用圆括号
    'arrow-body-style': 0, // 要求箭头函数体使用大括号
    'no-confusing-arrow': [2, { allowParens: true }], // 禁止在可能与比较操作符相混淆的地方使用箭头函数
    'object-shorthand': [2, 'properties', { avoidQuotes: true }], // 要求或禁止对象字面量中方法和属性使用简写语法
    'prefer-destructuring': 1, // 解构

    /**
     *  Node.js and CommonJS
     *  这些规则是关于Node.js 或 在浏览器中使用CommonJS 的
     *  https://cn.eslint.org/docs/rules/#nodejs-and-commonjs
     */
    'handle-callback-err': [2, '^(err|error)$'], // 有err/error必须处理异常
    'no-new-require': 2, // new require(xxx)->no
    'no-path-concat': 2, // __dirname和__filename禁止string拼接
    'global-require': 0, // 要求 require() 出现在顶层模块作用域中

    /**
     *  eslint-plugin-react
     *  https://github.com/yannickcr/eslint-plugin-react/tree/master/docs/rules
     */
    'react/jsx-uses-react': 2,
    'react/prefer-es6-class': 1, // 禁止使用ES5语法创建React组件
    'react/prefer-stateless-function': 1, // 组件没有状态或是没有引用refs，推荐使用无状态组件(函数声明)而不是类
    'react/jsx-pascal-case': 2, // 组件名称使用帕斯卡命名法
    'react/jsx-closing-bracket-location': 1, // 自关闭的jsx标签代码对齐格式
    'react/jsx-tag-spacing': 1, // 自关闭的标签前加一个空格
    'react/jsx-curly-brace-presence': 1, // 不需要多余的 去掉props和children多余的花括号,如title={"标题"}和<div>{"标题"}</div>
    'react/self-closing-comp': [1, { component: true, html: false }], // 没有子元素的标签总是自关闭标签
    'react/jsx-curly-spacing': 1, // 不要在props的{}里两边加空格
    'react/jsx-equals-spacing': 1, // props属性不允许等号两边加空格
    'react/jsx-indent-props': [1, 2], // props缩进格式
    'react/jsx-uses-vars': 2, // 防止在JSX中使用的变量被错误地标记为未使用
    'react/jsx-boolean-value': 1, // boolean类型props属性不需要加 ={true}
    'react/no-string-refs': 1, // ref里使用回调函数
    'react/no-children-prop': 1, // 不允许在props添加children属性
    'react/no-danger-with-children': 1, // 阻止chidren和dangerouslySetInnerHTML={{ __html: "HTML" }}同时使用
    'react/no-deprecated': 1, // 阻止使用旧版本即将废弃的api
    'react/no-did-mount-set-state': 1, // 阻止在componentDidMount中使用this.setState()
    'react/no-did-update-set-state': 1, // 阻止在componentDidUpdate中使用this.setState()
    'react/no-direct-mutation-state': 1, // 禁止直接修改state
    'react/no-find-dom-node': 0, // 禁用 findDOMNode
    'react/no-is-mounted': 1, // 禁用 isMounted
    'react/no-multi-comp': 0, // 阻止一个文件定义多个组件
    'react/no-typos': 1, // 检测错误的组件生命周期名称,以及组件静态属性,主要是大小写
    'react/no-this-in-sfc': 0, // 阻止在无状态组件中使用this
    // 'react/no-redundant-should-component-update': 1, // 阻止在纯组件中使用shouldComponentUpdate
    'react/no-unescaped-entities': 1, // jsx防止无效字符,比如  } " '
    'react/no-unknown-property': 1, // 防止使用未知的DOM属性
    'react/no-unused-state': 1, // 防止定义了state却未使用
    'react/no-will-update-set-state': 1, // 防止在componentWillUpdate中使用this.setState
    'react/jsx-no-duplicate-props': [1], // 重复props属性
    'react/jsx-key': [1], // 渲染array需要key
    'react/jsx-indent': [1, 2], // jsx缩进2个空格
    'react/react-in-jsx-scope': 1, // 防止未引入react就使用jsx
    'react/style-prop-object': 1, // props的style属性必须是个object
    'react/void-dom-elements-no-children': 1, // 自关闭的element不允许添加children ,如<br/>
    'react/jsx-no-bind': 0,
    'react/jsx-first-prop-new-line': 0,
    'react/jsx-filename-extension': 0,
    'react/destructuring-assignment': 0,
    'react/prop-types': 0,
    'react/forbid-prop-types': 0,
    'react/jsx-props-no-spreading': 0,
    'react/static-property-placement': 0,

    /**
     *  eslint-plugin-import
     *  https://github.com/benmosher/eslint-plugin-import/tree/master/docs/rules
     */
    'import/no-unresolved': 0,
    'import/no-extraneous-dependencies': 0,
    'import/prefer-default-export': 0,
    'import/no-mutable-exports': 0,
    'import/no-named-as-default': 0,
    // 'import/extensions': 0,
    /**
     *  eslint-plugin-jsx-a11y
     *  https://github.com/evcohen/eslint-plugin-jsx-a11y/tree/master/docs/rules
     */
    'jsx-a11y/anchor-is-valid': 0,
    'jsx-a11y/click-events-have-key-events': 0,
    'jsx-a11y/no-static-element-interactions': 0,
    'jsx-a11y/mouse-events-have-key-events': 0,
    'jsx-a11y/no-noninteractive-element-interactions': 0,
  }
}
