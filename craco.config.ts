// 使用less插件
const CracoLessPlugin = require('craco-less')
// 使用alias插件
const CracoAliasPlugin = require('craco-alias')

const config = () => ({
  // 编写修改webpack配置，可以参考craco，和webpack的语法
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            // 修改主题色
            modifyVars: { '@primary-color': '#4578FA' },
            javascriptEnabled: true,
          },
        },
      },
    },
    {
      plugin: CracoAliasPlugin,
      options: {
        source: 'tsconfig',
        baseUrl: '.',
        tsConfigPath: './tsconfig.path.json',
      },
    },
  ],
  babel: {
    // 支持装饰器模式语法
    plugins: [['@babel/plugin-proposal-decorators', { legacy: true }]],
  },
})

export default config
